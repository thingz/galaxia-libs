��          �               �     �     �          %  K   7  >   �     �     �  E   �  _   ?     �  ,   �     �  %   �  $        D  $   c     �     �     �     �  &   �  <     !   Q     s     �      �  �  �  %   9     _     x     �  W   �  +     ,   2     _  V   ~  c   �     9	  1   K	  0   }	  0   �	  -   �	     
  '   '
     O
  '   `
     �
  #   �
  (   �
  C   �
  3   9     m     �  !   �   A string containing the message Access point name Access point password Access point ssid An array of Network objects. Each has a ssid attribute and a rssi attribute Close the connection with the current client of the TCP server Connect to IP and send data Connect to an access point Filter request by IP. Requests from other IPs than IP will be ignored If a client is already connected, keep the connection and try to receive from this client again Module contents Receive an HTTP request from the HTTP server Receive data from TCP server Respond to a client of the TCP server Respond to client of the HTTP server Scan wifi nearby wifi networks Start an access point on the Galaxia The HTTP request The IP address to connect to The port to connect to The port used by the TCP server The static IP to assign to the Galaxia set how long to wait before returning if no data is received set the end of message characters the data to be send the response wait until something is received Project-Id-Version: Galaxia libs 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2022-01-27 16:23+0100
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: fr_FR
Language-Team: fr_FR <LL@li.org>
Plural-Forms: nplurals=2; plural=(n > 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.9.1
 Le message reçu sous forme de string Le nom du point d'accès Mot de passe du point d'accès SSID du point d'accès Un tableau d'objets Network. Chaque objet contient un attribut ssid et un attribut rssi Ferme la connexion du client du serveur TCP Se connecter à une ip et envoyer un message Connexion à un point d'accès Filtrer les clients par IP. Seulement une connexion de cette adresse IP sera acceptée Si un client est déjà connecté, garder sa connexion et essayer de recevoir de nouvelles données Contenu du module Recevoir une requête HTTP depuis le serveur HTTP Recevoir des données d'un client du serveur TCP Répondre à un message du client du serveur TCP Répondre à la requête HTTP du serveur HTTP Scanner les réseaux Wifi Lancer un point d'accès sur la Galaxia La requête HTTP L'addresse IP sur laquelle se connecter Le port sur lequel se connecter Le port utilisé par le serveur TCP L'IP statique à attribuer à la Galaxia Choisir combien de secondes attendre si aucune donnée n'est reçue Choisir la chaîne de caractères de fin de message Le message à envoyer La réponse Bloquer tant que rien n'est reçu 