.. Galaxia libs documentation master file, created by
   sphinx-quickstart on Wed May 12 11:41:07 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Galaxia libs's documentation!
========================================

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   source/modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`